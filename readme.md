# README

In order to run this tool, you shold be familiar with the instructions from the Client. Some of them are printed copied here:

## Steps

1. Type in your browser the search UI web-address (URL), for staging environment, e.g. https://stage-search.cloud.hhs.gov/searchblox/hhs/index.html 
1. Run a search query, for example "health".
1. Grab the URL from the browser (https://stage-search.cloud.hhs.gov/searchblox/hhs/index.html?query=health&page=1&pagesize=10&sort=relevance&sortdir=desc&adsCname=HHS&adsDisplay=true&cname=hhsgov_only&default=AND&tune=true&tune.0=10&tune.1=8&tune.2=2&tune.3=5&tune.4=365&tune.5=30 )
1. **This is where the tool comes in**. Paste the URL from previous step in the tool.
1. The tool will generate the HTML that you need, and also allow you to download an .HTML file by right-clicking at on the button at the bottom of the page.

## Running the Tool
### 1. Heroku
- https://search-form-generator.herokuapp.com/
### 2. Locally
This has some benefits, like being able to see all of the files that are generated without having to download them.
1. Download the repo from https://gitlab.com/bluejayfrost/bah-hhs-sfg. 
1. Navigate to the main directory in your CLI
1. Run `php -S localhost:9001`
1. Visit in your browser: [http://localhost:9001](http://localhost:9001)
