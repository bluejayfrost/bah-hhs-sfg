<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="assets/prism.css">
  <script src="assets/prism.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style>
.card, textarea {
    margin: 20px 0;
}
.btn {
    margin-bottom: 20px;
}
</style>
</head>
<body>
<?php 
//$queryurl = 'https://stage-search.cloud.hhs.gov/searchblox/stopbullying/index.html?query=health&page=1&pagesize=10&sortdir=desc&sort=relevance&adsCname=stopbullying&adsDisplay=true&cname=stopbullying&default=AND&f.language.filter=en&facet.field=language&tune=true&tune.0=1&tune.1=0&tune.2=0&tune.3=0&tune.4=365&tune.5=100';

// Set up PHP variables and sanitize
if(isset($_POST['queryUrl'])) {
    $queryUrl = filter_var($_POST['queryUrl'], FILTER_SANITIZE_STRING);

    // rudimentary check for query string parameters
    if(substr_count($queryUrl, "&") < 3) {
        $errMsg = "Your URL string should have several search parameters in it, such as <pre>...&term=value&label=value...</pre>";
    }

    // make sure that the URL has a query string (signified by a '?')
    if(substr_count($queryUrl, '?') !== 1) {
        $errMsg = "Your URL string should have <b>one</b> question mark that signifies the start of the query string.";
    }

    // check for line breaks
    if(substr_count($queryUrl, PHP_EOL) !== 0) {
        $errMsg = "Please remove line breaks.";
    }
    
}

?>

<div class="jumbotron">
    <div class="container">
        <h1>SearchBlox HTML Form Generator v2</h1>
    </div>
</div>
<div class="container">
Enter the Search Query String URL for which you'd like to create a search box.
<form action="" method="post">
    <textarea name="queryUrl" type="text" class="w-100" rows="3"><?php if(isset($queryUrl)) echo $queryUrl; ?></textarea>
    <br>
    <input class="btn btn-primary" type="submit">
</form>
</div>
<div class="container">

    <?php if($errMsg): ?>
        <div class="alert alert-danger" role="alert"><?php echo $errMsg; ?></div>
    <?php else: ?>

    <h2>Generated HTML Form</h2>
        <pre><code class="language-markup"><?php 
                    if(isset($queryUrl)) {

                        // Split queryURL into two parts seperated by ?
                        list($baseUrl, $queryStringPairs) = explode("?",$queryUrl);

                        $firstline = "<form action='$baseUrl' method='get'>" . PHP_EOL;
                        $secondline = "<input type='text' name='query' value=''>" . PHP_EOL . PHP_EOL . "<!-- BEGIN HIDDEN INPUTS -->" . PHP_EOL;
                        echo htmlspecialchars($firstline);
                        echo "&#9;" . htmlspecialchars($secondline);

                        // Make $querystring into an array
                        $arrayOfQueryStringPairs = (explode("&",$queryStringPairs));

                        // Get name of form
                        function get_string_between($string, $start, $end){
                            $string = " ".$string;
                            $ini = strpos($string,$start);
                            if ($ini == 0) return "";
                            $ini += strlen($start);   
                            $len = strpos($string,$end,$ini) - $ini;
                            return substr($string,$ini,$len);
                        }
                        $formName = get_string_between($baseUrl, 'searchblox/', '/index'); 
                        $formName = strtoupper($formName);

                        $cName = get_string_between($queryStringPairs, '&cname=', '&');
                        $cName = strtoupper($cName);
                
                        // take the array, remove first label/value pair, and run a foreach on the remaining
                        foreach (array_slice($arrayOfQueryStringPairs,1) as $arrayOfQueryStringPair) {

                        // loop through label/value pairs and assign them variables
                            list($label, $value) = explode("=",$arrayOfQueryStringPair);

                        // print the array
                            echo "&#9;" . htmlspecialchars("<input type='hidden' name='$label' value='$value'> ") . PHP_EOL;
                        }

                        // add input button
                        $penultimateLine = "<!-- END OF HIDDEN INPUTS -->" . PHP_EOL . PHP_EOL . PHP_EOL . "<!-- REMOVE THE FOLLOWING LINE: IT IS FOR TESTING PURPOSES ONLY -->" . PHP_EOL . "<input type='submit' value='Go'>" . PHP_EOL . "<!-- REMOVE THE LINE ABOVE -->" . PHP_EOL . PHP_EOL;
                        $lastLine = "</form>";
                        echo "&#9;" . htmlspecialchars($penultimateLine) . htmlspecialchars($lastLine) . PHP_EOL;
                } else {
                    echo "Please enter a URL above, per the instructions.";
                }
            ?></code></pre>

    <?php if(isset($queryUrl)) { ?>
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Form Name/CNAME</h3>
                <div class="card-text"><?php echo "$formName/$cName" ?></div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Base Url:</h3>
                <div class="card-text"><?php echo "<a href='$baseUrl' target='_blank'>$baseUrl</a>" ?></div>
            </div>
        </div>

        <?php 

            // create file
            $fileName = "$formName-$cName.html";

            // start file creation
            $fileContents = $firstline . $secondline;

            // create new loop
            foreach (array_slice($arrayOfQueryStringPairs,1) as $arrayOfQueryStringPair) {

            // loop through label/value pairs and assign them variables
                list($label, $value) = explode("=",$arrayOfQueryStringPair);

            // add each array item to the file contents
                $fileContents .= "<input type='hidden' name='$label' value='$value'>" . PHP_EOL;

            }

            // finish assembling file contents
            $extraInfo = PHP_EOL . "<p><b>Form Name/CNAME:</b> $formName/$cName</p>" . PHP_EOL;
            $extraInfo2 = "<p><b>Base Url:</b> <a href='$baseUrl' target='_blank'>$baseUrl</a></p>" . PHP_EOL;
            $fileContents .= $penultimateLine . $lastLine . $extraInfo . $extraInfo2;

            // create file
            file_put_contents($fileName, $fileContents);

            echo "<h1>File name: $fileName</h1>";

        ?>
        <a class="btn btn-primary text-light" target="_blank" href="<?php echo $fileName; ?>">View <?php echo $fileName ?></a>
        <p><em>To download HTML file, right click the button above and select <b>Save Link As</b> (Chrome) or your browser's equivalent. </em></p>

    <?php } ?>

    <?php endif; ?>
</div>
